import angular from 'angular';
import bootstrap from 'angular-ui-bootstrap';
import uiRouter from 'angular-ui-router';

import csNav from './nav.component';

export default angular
    .module('app.nav', [
        bootstrap, uiRouter
    ])
    .directive({csNav})
    .name;
