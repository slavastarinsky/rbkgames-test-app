import template from './nav.jade';

class NavigationController {
    /**
     * @param $window
     * @param $state
     */
    constructor($window, $state) {
        "ngInject";

        this.$window = $window;
        this.state = $state;
    }
}

export default function ($window, $state) {
    "ngInject";

    return {
        restrict: "E",
        template,
        scope: {},
        controller: NavigationController,
        controllerAs: 'vm'
    }
}
