import angular from 'angular';
import bootstrap from 'angular-ui-bootstrap';
import uiRouter from 'angular-ui-router';
import {api} from 'common/common';
import config from 'app-config';

import nav from 'app/nav/nav.js';
import exchange from 'app/exchange/exchange.js';

import csApp from './app.component.js';
import states from './app.states.js';

export default angular
    .module('app', [
        bootstrap, uiRouter,
        api,
        nav, exchange
    ])
    .constant(config)
    .config(function (apiProvider, $logProvider, $locationProvider, APP_DEBUG, API_ENDPOINT) {
        "ngInject";

        $logProvider.debugEnabled(APP_DEBUG);
        apiProvider.setEndpoint(API_ENDPOINT);
        $locationProvider.html5Mode(true);
    })
    .config(states)
    .run(function ($state) {
        "ngInject";
    })
    .directive({csApp})
    .name;
