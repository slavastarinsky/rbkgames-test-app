import angular from 'angular';
import {api} from 'common/common';

import exchangeManager from './manager.service';

export default angular
    .module('app.exchange.manager', [
        api
    ])
    .service({exchangeManager})
    .name;