import {List} from 'common/common';

export default class ExchangeManager {

    constructor(api, $log, $q) {
        "ngInject";

        this.api = api;
        this.$log = $log;
        this.q = $q;
    }

    getCurrenciesList(filters={}) {

        return this.api.get('/currencies', {
            params: filters
        }).then((response) => {
            return response.data;
        }, (reason) => {
            this.$log.error(reason);
            throw new Error('Unable to fetch flights');
        });
    }

    exchange(date, amount, currencyFrom, currencyTo) {
        return this.api.post('/exchange', {
            amount: amount,
            from: currencyFrom,
            to: currencyTo,
            date: date
        }).then((response) => {
            return response.data;
        }, (reason) => {
            this.$log.error(reason);
            throw new Error('Unable to get result');
        });
    }

    $deserializeCurrency(currency) {
        return currency;
    }
}

/**
 * @param {Date} date
 * @returns {Date}
 */
function serializeDate(date) {
    return date.toISOString().replace(/\.\d+Z$/, 'Z');
}

/**
 * @param {*} obj
 * @param {string} prop
 */
function deserializerDate(obj, prop) {
    if (!obj.hasOwnProperty(prop)) {
        return null;
    }

    obj[prop] = new Date(Date.parse(obj[prop].replace(/(\.|\+)[0\:]{4,5}/, 'Z')));
}
