import template from './exchange.jade';

class ExchangeController {
    constructor(exchangeManager, $parse, $q) {
        "ngInject";

        this.manager = exchangeManager;

        this.setDefaults();
        this.getCurrencies();
    }

    getCurrencies(filters = {}) {
        this.currencies = [];
        this.loading = this.manager.getCurrenciesList(filters).then((currencies) => {
            console.log(currencies.length);
            if (0 == currencies.length) {
                this.emptyDBMessage = true;
            } else {
                this.currencies = currencies;
            }
        }, (response) => {
            this.errorMessage = response.status === 400 ?
                response.data.message : 'No currencies yet';
        }).finally(() => {
            this.loading = null;
        })
    }

    exchange(date, amount, currencyFrom, currencyTo) {
        this.result = "";

        this.manager.exchange(date, amount, currencyFrom, currencyTo).then((result) => {
            console.log(result);
            this.result = result;
        }, (response) => {
            this.errorMessage = response.status === 400 ?
                response.data.message : 'Can\'t get result';
        }).finally(() => {
            this.loading = null;
        })
    }

    setDefaults() {
        this.date = new Date();
        this.amount = 100;
        this.from = 'USD';
        this.to = 'CAD';
    }

    //setDefaults() {
    //    var defaults = [
    //        { name: 'date', value: new Date() },
    //        { name: 'amount', value: 100 },
    //        { name: 'from', value: 'USD' },
    //        { name: 'to', value: 'CAD' }
    //    ];
    //
    //    defaults.forEach(defaults, function(option) {
    //        var model = $parse(option.name);
    //        model.assign(this, option.value);
    //    });
    //}
}

export default function () {

    return {
        restrict: 'E',
        template,
        controller: ExchangeController,
        controllerAs: 'vm'
    };
}
