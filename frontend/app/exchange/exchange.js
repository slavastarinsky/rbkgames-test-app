import angular from 'angular';
import uiRouter from 'angular-ui-router';
import {api} from 'common/common';
import exchangeManager from 'app/exchange/manager/manager'

import csExchange from './exchange.component.js';

export default angular
    .module('app.exchange', [
        exchangeManager, uiRouter, api
    ])
    .directive({csExchange})
    .name;
