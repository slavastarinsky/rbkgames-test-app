

export default function ($stateProvider, $urlRouterProvider) {
    "ngInject";

    $stateProvider
        .state('exchange', {
            url: '/',
            template: '<cs-exchange></cs-exchange>'
        });

    $urlRouterProvider.otherwise('/');
}