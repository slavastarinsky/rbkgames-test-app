import template from './date.jade';
import moment from 'moment';

class DateController {
  constructor(dateFilter) {
    "ngInject";
    this.dateFilter = dateFilter;
    this.dateFormat = 'dd/MMM/yy';
    this.clearData();
  }

  prepareSelectedValue() {
    if (this.filterType === 'withing') {
      return `Withing the last ${this.within.val} ${this.within.unit}`
    }

    if (this.filterType === 'morethan') {
      return `More than ${this.moreThan.val} ${this.moreThan.unit} ago`
    }

    if (this.filterType === 'range') {
      var dateRange = (prop) => {
        return this.dateFilter(this.dateRange[prop], this.dateFormat)
      };

      return `Between ${dateRange('from')} and ${dateRange('to')}`
    }

    return 'All';
  }

  prepareModelValue() {
    if (this.filterType === 'withing') {
      return {
        from: moment().subtract(this.within.val, this.within.unit.toLowerCase()).toDate()
      };
    }

    if (this.filterType === 'morethan') {
      return {
        to: moment().subtract(this.moreThan.val, this.moreThan.unit.toLowerCase()).toDate()
      };
    }

    if (this.filterType === 'range') {
      return {
        from: this.dateRange.from,
        to: this.dateRange.to
      }
    }

    return {};
  }

  clearData() {
    this.popoverOpened = false;
    this.filterType = '';
    this.within = {unit: 'Hours'};
    this.moreThan = {unit: 'Hours'};
    this.dateRange = {};
    this.selectedValue = this.prepareSelectedValue();
  }

  clear() {
    this.clearData();
    this.applyFilter();
  }

  isValid() {
    if (this.filterType === 'withing' && this.within.val > 0) {
      return true;
    }

    if (this.filterType === 'morethan' && this.moreThan.val > 0) {
      return true;
    }

    if (this.filterType === 'range') {
      return angular.isDate(this.dateRange.from) && angular.isDate(this.dateRange.to);
    }

    return true;
  }

  close() {
    this.popoverOpened = false;
  }

  applyFilter() {
    if (!this.isValid()) {
      return;
    }

    this.selectedValue = this.prepareSelectedValue();
    this.onChange({dateRange: this.prepareModelValue()});

    this.close();
  }
}

export default function ($document, $timeout) {
  "ngInject";

  return {
    template,
    controller: DateController,
    controllerAs: 'vm',
    scope: {
      label: '@'
    },
    bindToController: {
      onChange: '&'
    },
    restrict: 'E',
    link: function (scope, el, attr, ctrl) {
        $document.bind('click', closeActivePopover);

        scope.$on('$destroy', () => {
            $document.unbind('click', closeActivePopover);
        });

        scope.$watch(() => ctrl.popoverOpened, (opened) => {
            if (opened) {
                setTimeout(() => {
                    var $popover = $document[0].querySelector('.popover.fade.in');
                    $popover.addEventListener('click', (e) => e.stopPropagation(), false);
                }, 0);
            }
        });

        function closeActivePopover(e) {
            if (!isDescendant(el[0], e.target) && ctrl.popoverOpened) {
                $timeout(() => {
                    ctrl.close()
                }, 0);
            }
        }
    }
  };
};

function isDescendant(parent, child) {
    var node = child;
    while (node !== null) {
        if (node === parent) {
            return true;
        }
        node = node.parentNode;
    }
    return false;
}
