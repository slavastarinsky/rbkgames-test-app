import angular from 'angular';
import bootstrap from 'angular-ui-bootstrap';

import dxDateFilter from './date.component';

export default angular
    .module('admin.filters.date', [
      bootstrap
    ])
    .directive({dxDateFilter})
    .name;
