
export default class List {
    constructor(source) {
        this.list = [];
        this.refresh(source);
    }

    refresh(source) {
        this.$initializeList(source);
    }

    get isEmpty() {
        return !this.loading && this.list.length === 0;
    }

    $initializeList(source) {
        this.loading = false;
        this.list = [];
        if (!Array.isArray(source)) {
            this.loading = true;
            this.promise = source;
            source.then((list) => {
                console.log('loaded list', list);
                if (this.promise === source) {
                    this.list = list;
                }
            }).finally(() => {
                this.loading = false;
                this.promise = null;
            });
        }
    }
}
