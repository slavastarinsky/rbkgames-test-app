import angular from 'angular';
import api from './api.provider.js';

export default angular
  .module('common.api', [])
  .provider({api})
  .name;
