
/** @ngInject **/
export default function () {
  var _endpoint;

  this.setEndpoint = function (endpoint) {
    _endpoint = endpoint;
  };

  this.$get = function ($http, $q, $location) {
    "ngInject";

    var methods = {};

    var mergeHeaders = function (config) {
      config.headers = config.headers || {};
      return angular.extend(config.headers, methods.defaults.headers || {});
    };

    var api = function (config) {
      config = angular.extend(config, methods.defaults);
      config.url = methods.getEndpoint() + config.url;
      mergeHeaders(config);

      return $http(config);
    };

    ['GET', 'DELETE'].forEach(function (method) {
      methods[method.toLowerCase()] = function (uri, config) {
        config = config || {};
        config.method = method;
        config.url = uri;

        return api(config);
      };
    });

    ['POST', 'PUT', 'PATCH'].forEach(function (method) {
      methods[method.toLowerCase()] = function (uri, data, config) {
        config = config || {};
        data = angular.copy(data); // clean up and copy data to make it immutable
        config.method = method;
        config.url = uri;
        config.data = data;

        return api(config);
      };
    });

    methods.defaults = {
      headers: {
        'Content-Type': 'application/json'
      }
    };

    methods.getEndpoint = function () {
      if (!/^\w+:\/\//.test(_endpoint)) {
          console.log('$location.port()', $location.port());
          _endpoint = $location.protocol() + "://" + $location.host() + ($location.port() ? (':' + $location.port()) : '') + _endpoint;
      }

      return _endpoint;
    };

    methods.stripResponseData = stripResponseData;

    methods.repeatRejection = repeatRejection;

    return methods;

    function stripResponseData (response) {
      return response.data;
    }

    function repeatRejection (reason) {
      return $q.reject(reason);
    }
  }
}
