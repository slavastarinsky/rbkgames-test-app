import angular from 'angular';
import uiRouter from 'angular-ui-router';
import 'core-js/modules/es6.object.assign';

export default angular
    .module('common.stateUpdater', [uiRouter])
    .factory({stateUpdater})
    .name;

function stateUpdater($state) {
    "ngInject";

    return function (overrideParams = {}, refresh = false) {
        console.log('update state', overrideParams);
        $state.go($state.current.name, Object.assign(
            $state.params,
            overrideParams
        ), {
            notify: !refresh
        }).then(() => true, (reason) => console.log(reason))
    }
}
