<?php
/**
 * Created by PhpStorm.
 * User: slavastarinsky
 * Date: 6/16/16
 * Time: 21:17
 */

namespace RBKGTest\Domain\Exchange;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="rate", uniqueConstraints={
 *     @ORM\UniqueConstraint(name="date_code", columns={"date", "currency_id"})
 * })
 */
class Rate
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var \DateTime
     * @ORM\Column(type="date")
     */
    private $date;

    /**
     * @var Currency
     * @ORM\ManyToOne(targetEntity="RBKGTest\Domain\Exchange\Currency")
     */
    private $currency;

    /**
     * @var float
     * @ORM\Column(type="float")
     */
    private $value;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @var booleab
     * @ORM\Column(type="boolean")
     */
    private $active;

    public function __construct(\DateTime $date, Currency $currency, $rate)
    {
        $this->date = $date;
        $this->currency = $currency;
        $this->value = $rate;
        $this->active = true;
        $this->created = new \DateTime();
    }

    /**
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param \DateTime $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return Currency
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param Currency $currency
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;
    }

    /**
     * @return float
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param float $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    public function toArray()
    {
        return [
            'id' => $this->id,
            'currency' => $this->currency->getCode(),
            'rate' => $this->value
        ];
    }
}