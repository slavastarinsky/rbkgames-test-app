<?php
/**
 * Created by PhpStorm.
 * User: slavastarinsky
 * Date: 6/20/16
 * Time: 15:08
 */

namespace RBKGTest\Domain\Exchange;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="currency", uniqueConstraints={
 *     @ORM\UniqueConstraint(name="code", columns={"code"})
 * })
 */
class Currency
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $code;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    private $created;

    public function __construct($code)
    {
        $this->code = $code;
        $this->created = new \DateTime();
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    public function toArray()
    {
        return [
            'id' => $this->id,
            'code' => $this->code
        ];
    }
}