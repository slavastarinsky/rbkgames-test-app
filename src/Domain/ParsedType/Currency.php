<?php
/**
 * Created by PhpStorm.
 * User: slavastarinsky
 * Date: 6/20/16
 * Time: 13:20
 */

namespace RBKGTest\Domain\ParsedType;

class Currency
{
    /**
     * @var string
     */
    private $id;

    /**
     * @var float
     */
    private $rate;

    public function __construct($id, $rate)
    {
        $this->id = $id;
        $this->rate = $rate;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return float
     */
    public function getRate()
    {
        return $this->rate;
    }

    /**
     * @param float $rate
     */
    public function setRate($rate)
    {
        $this->rate = $rate;
    }
}