<?php
/**
 * Created by PhpStorm.
 * User: slavastarinsky
 * Date: 6/20/16
 * Time: 13:21
 */

namespace RBKGTest\Domain\ParsedType;

class CurrenciesList
{
    /**
     * @var Currency[]
     */
    private $currencies;

    /**
     * @return Currency[]
     */
    public function getCurrencies()
    {
        return $this->currencies;
    }

    /**
     * @param $currencies
     */
    public function setCurrencies($currencies)
    {
        if (!$currencies['Currency']) {
            $this->currencies = [];
        }

        foreach($currencies['Currency'] as $currency) {
            $this->currencies[] = new Currency(
                $currency['ID'], $currency['Rate']
            );
        }
    }
}