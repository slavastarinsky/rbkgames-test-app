<?php
/**
 * Created by PhpStorm.
 * User: slavastarinsky
 * Date: 6/16/16
 * Time: 21:13
 */

namespace RBKGTest\Controller;

use RBKGTest\Application\DTO\Exchange\ExchangeRequest;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration as Config;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class ExchangeController extends Controller
{
    /**
     * @Config\Route("exchange", name="api_get_exchange")
     * @Config\Method("POST")
     */
    public function getExchange(Request $request)
    {
        $from = $this->get('exchange_registry')->getCurrencyByCode($request->get('from'));
        $to = $this->get('exchange_registry')->getCurrencyByCode($request->get('to'));

        $request = new ExchangeRequest($from, $to, [
            'date' => $request->get('date'),
            'amount' => $request->get('amount')
        ]);

        if (!$this->get('exchange_registry')->checkRatesForDate($request->getDate())) {
            $this->get('exchange_registry')->parse($request->getDate());
            $this->get('doctrine.orm.entity_manager')->flush();
        }

        $result = $this->get('exchange_registry')
            ->exchange($request);

        return new JsonResponse($result);
    }

    /**
     * @Config\Route("currencies", name="api_get_currencies_list")
     * @Config\Method("GET")
     */
    public function getCurrenciesList()
    {
        return new JsonResponse(
            $this->get('exchange_registry')->getCurrenciesList()
        );
    }

}