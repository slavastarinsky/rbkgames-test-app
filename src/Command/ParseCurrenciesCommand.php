<?php
/**
 * Created by PhpStorm.
 * User: slavastarinsky
 * Date: 6/17/16
 * Time: 14:53
 */

namespace RBKGTest\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ParseCurrenciesCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('rbkg:exchange:parse')
            ->setDescription('Parse currencies for the date')
            ->addArgument('date', InputArgument::REQUIRED, 'Date for parsing')
        ;
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $date = new \DateTime($input->getArgument('date'));

        $this->getContainer()->get('exchange_registry')->parse($date);

        $this->getContainer()->get('doctrine.orm.entity_manager')->flush();

        $output->writeln(
            sprintf('<info>Currencies for %s parsed successfully!</info>', $date->format(\DateTime::ATOM))
        );
    }
}