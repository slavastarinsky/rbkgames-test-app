<?php
/**
 * Created by PhpStorm.
 * User: slavastarinsky
 * Date: 6/17/16
 * Time: 14:53
 */

namespace RBKGTest\Command;

use RBKGTest\Application\DTO\Exchange\ExchangeRequest;
use RBKGTest\Domain\Exchange\Currency;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ExchangeCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('rbkg:currency:exchange')
            ->setDescription('Exchange for the date')
            ->addArgument('date', InputArgument::REQUIRED, 'Date for parsing')
            ->addArgument('from', InputArgument::REQUIRED, 'Currency convert from')
            ->addArgument('to', InputArgument::REQUIRED, 'Currency convert to')
            ->addArgument('amount', InputArgument::OPTIONAL, 'Total amount')
        ;
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var Currency $from */
        $from = $this->getContainer()->get('exchange_registry')
            ->getCurrencyByCode($input->getArgument('from'));
        /** @var Currency $to */
        $to = $this->getContainer()->get('exchange_registry')
            ->getCurrencyByCode($input->getArgument('to'));

        $request = new ExchangeRequest($from, $to, [
            'date' => $input->getArgument('date'),
            'amount' => (int) $input->getArgument('amount')
        ]);

        $result = $this->getContainer()->get('exchange_registry')->exchange($request);

        $output->writeln(
            sprintf('<info>%d %s = %f %s</info>',
                $request->getAmount(),
                $from->getCode(),
                $result,
                $to->getCode()
            )
        );
    }
}