<?php
/**
 * Created by PhpStorm.
 * User: slavastarinsky
 * Date: 6/16/16
 * Time: 21:23
 */

namespace RBKGTest\Application;

use Doctrine\ORM\EntityNotFoundException;
use RBKGTest\Application\DTO\Exchange\ExchangeRequest;
use RBKGTest\Domain\Exchange\Currency;
use RBKGTest\Domain\Exchange\Rate;
use RBKGTest\Infrastructure\Parser\AbstractParser;
use RBKGTest\Infrastructure\Repository\CurrencyRepository;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class ExchangeRegistry
{
    /**
     * @var CurrencyRepository
     */
    private $currencyRepository;

    /**
     * @var AbstractParser
     */
    private $parser;

    /**
     * @var Serializer
     */
    private $serializer;

    public function __construct(
        CurrencyRepository $currencyRepository,
        AbstractParser $parser
    ) {
        $this->currencyRepository = $currencyRepository;
        $this->parser = $parser;

        $this->serializer = new Serializer(
            [new ObjectNormalizer()],
            [new XmlEncoder(), new JsonEncoder()]
        );
    }

    public function exchange(ExchangeRequest $request)
    {
        /** @var Rate $fromRate */
        $fromRate = $this->currencyRepository->getRateFor($request->getFrom(), $request->getDate());
        /** @var Rate $toRate */
        $toRate = $this->currencyRepository->getRateFor($request->getTo(), $request->getDate());

        return (int) $request->getAmount() * $toRate->getValue() / $fromRate->getValue();
    }

    public function parse(\DateTime $date)
    {
        $xml = $this->parser->parse($date);
        if (!($list = $this->serializer->deserialize($xml, 'RBKGTest\Domain\ParsedType\CurrenciesList', 'xml'))) {
            throw new \Exception("No data for the date");
        }

        /** @var \RBKGTest\Domain\ParsedType\Currency $rate */
        foreach ($list->getCurrencies() as $rate) {
            if (!($currency = $this->currencyRepository->findCurrency($rate->getId()))) {
                $currency = $this->currencyRepository->saveCurrency(new Currency($rate->getId()));
            }
            $this->currencyRepository->save(
                new Rate($date, $currency, $rate->getRate())
            );
        }
    }

    public function getCurrenciesList()
    {
        return array_map(function(Currency $currency) {
            return $currency->toArray();
        }, $this->currencyRepository->getCurrenciesList());
    }

    public function getCurrencyByCode($code)
    {
        if (!($currency = $this->currencyRepository->findCurrency($code))) {
            throw new EntityNotFoundException();
        }
        return $currency;
    }

    public function checkRatesForDate(\DateTime $date)
    {
        /** @var Rate $fromRate */
        return (bool) $this->currencyRepository->getRatesCountForDate($date);
    }
}

