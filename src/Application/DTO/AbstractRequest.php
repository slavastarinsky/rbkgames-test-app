<?php
/**
 * Created by PhpStorm.
 * User: slavastarinsky
 * Date: 6/20/16
 * Time: 15:50
 */

namespace RBKGTest\Application\DTO;

use Symfony\Component\OptionsResolver\OptionsResolver;

abstract class AbstractRequest
{
    public function __construct(array $data)
    {
        $options = new OptionsResolver();
        $this->configureOptions($options);

        $this->map($options, array_filter($data));
    }

    abstract protected function configureOptions(OptionsResolver $options);

    protected function map(OptionsResolver $options, array $data)
    {
        $resolvedData = $options->resolve($data);
        foreach ($options->getDefinedOptions() as $prop) {
            $this->$prop = $resolvedData[$prop];
        }
    }
}
