<?php
/**
 * Created by PhpStorm.
 * User: slavastarinsky
 * Date: 6/20/16
 * Time: 15:51
 */

namespace RBKGTest\Application\DTO\Exchange;

use RBKGTest\Application\DTO\AbstractRequest;
use RBKGTest\Domain\Exchange\Currency;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ExchangeRequest extends AbstractRequest
{
    /**
     * @var Currency
     */
    protected $from;

    /**
     * @var Currency
     */
    protected $to;

    /**
     * @var int
     */
    protected $amount;

    /**
     * @var \DateTime
     */
    protected $date;

    public function __construct(Currency $from, Currency $to, array $data)
    {
        parent::__construct($data);

        $this->from = $from;
        $this->to = $to;
    }

    protected function configureOptions(OptionsResolver $options)
    {
        $options
            ->setRequired([
                'amount',
                'date'
            ])
            ->setAllowedTypes([
                'amount' => 'int',
                'date' => 'string',
            ])
            ->setNormalizer('date', function ($options, $value) {
                return new \DateTime($value);
            })
            ->setNormalizer('amount', function($options, $value) {
                return (int) $value;
            })
            ->setDefault('amount', 1)
            ;
    }

    /**
     * @return Currency
     */
    public function getFrom()
    {
        return $this->from;
    }

    /**
     * @return Currency
     */
    public function getTo()
    {
        return $this->to;
    }

    /**
     * @return int
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

}