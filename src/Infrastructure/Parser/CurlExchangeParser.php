<?php
/**
 * Created by PhpStorm.
 * User: slavastarinsky
 * Date: 6/16/16
 * Time: 21:44
 */

namespace RBKGTest\Infrastructure\Parser;


class CurlExchangeParser extends AbstractParser
{
    const DATE_FORMAT = 'Ymd\THis';

    private $url = "https://www.bank.lv/vk/ecb.xml";

    public function __construct($url = null)
    {
        if ($url) {
            $this->url = $url;
        }
    }

    public function parse(\DateTime $date)
    {
        $date->setTime(9,0,0);

        $url = sprintf("%s?date=%s",
            $this->url,
            $date->format(self::DATE_FORMAT)
        );
        $xml = file_get_contents($url);
        return $xml;
    }
}