<?php
/**
 * Created by PhpStorm.
 * User: slavastarinsky
 * Date: 6/16/16
 * Time: 21:45
 */

namespace RBKGTest\Infrastructure\Parser;


abstract class AbstractParser
{
    public function parse(\DateTime $date) {}
}