<?php
/**
 * Created by PhpStorm.
 * User: slavastarinsky
 * Date: 6/16/16
 * Time: 21:24
 */

namespace RBKGTest\Infrastructure\Repository;

use Doctrine\ORM\EntityManagerInterface;

abstract class AbstractRepository
{
    /**
     * @var EntityManagerInterface
     */
    protected $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }
}