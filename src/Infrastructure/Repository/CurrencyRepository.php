<?php
/**
 * Created by PhpStorm.
 * User: slavastarinsky
 * Date: 6/16/16
 * Time: 21:24
 */

namespace RBKGTest\Infrastructure\Repository;

use RBKGTest\Domain\Exchange\Currency;
use RBKGTest\Domain\Exchange\Rate;

class CurrencyRepository extends AbstractRepository
{
    public function save(Rate $rate)
    {
        $this->em->persist($rate);
        return $rate;
    }

    public function saveCurrency(Currency $currency)
    {
        $this->em->persist($currency);
        return $currency;
    }

    public function getRateFor(Currency $currency, $date)
    {
        return $this->em->getRepository(Rate::class)
            ->findOneBy(['currency' => $currency, 'date' => $date]);
    }

    public function getCurrenciesList()
    {
        return $this->em->getRepository(Currency::class)
            ->findAll();
    }

    public function findCurrency($code)
    {
        return $this->em->getRepository(Currency::class)
            ->findOneBy(['code' => $code]);
    }

    public function getRatesCountForDate(\DateTime $date)
    {
        return count($this->em->getRepository(Rate::class)
            ->findBy(['date' => $date]));
    }
}