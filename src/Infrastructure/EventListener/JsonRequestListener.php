<?php

namespace RBKGTest\Infrastructure\EventListener;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;

class JsonRequestListener
{

    public function onRequest(GetResponseEvent $event)
    {
        if (!$event->isMasterRequest()) return;

        $request = $event->getRequest();
        if ('json' !== $request->getContentType() || !in_array($request->getMethod(), ['POST', 'PUT', 'PATCH'])) {
            return;
        }

        if (!($content = $request->getContent())) {
            return;
        }

        $requestData = json_decode($content, true);

        if (JSON_ERROR_NONE !== json_last_error()) {
            $event->setResponse(new JsonResponse([
                'error' => true,
                'message' => json_last_error_msg()
            ], JsonResponse::HTTP_BAD_REQUEST));

            return;
        }

        $request->request->add($requestData);
    }

}
