<?php
/**
 * ExceptionListener.php
 *
 * @author  starinsky
 * @created 9/29/15 12:46
 */

namespace RBKGTest\Infrastructure\EventListener;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;

class ExceptionListener
{
    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        $request = $event->getRequest();
        if ('json' !== $request->getContentType() || !in_array($request->getMethod(), ['POST', 'PUT', 'PATCH'])) {
            return;
        }

        $exception = $event->getException();
        $status = JsonResponse::HTTP_BAD_REQUEST;

        $event->setResponse(new JsonResponse([
            'error' => true,
            'message' => $exception->getMessage()
        ], $status));
    }
}