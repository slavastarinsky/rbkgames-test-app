'use strict';

const
  gulp     = require('gulp'),
  sourcemaps = require('gulp-sourcemaps'),
  webpack  = require('webpack-stream'),
  path     = require('path'),
  sync     = require('run-sequence'),
  serve    = require('browser-sync'),
  rename   = require('gulp-rename'),
  template = require('gulp-template'),
  fs       = require('fs'),
  plumber  = require('gulp-plumber'),
  inject   = require('gulp-inject'),
  named    = require('vinyl-named'),
  autoprefixer = require('autoprefixer'),
  httpProxy = require('http-proxy-middleware'),
  less     = require('gulp-less'),
  postcss  = require('gulp-postcss'),
  assets   = require('postcss-assets'),
  lodash   = require('lodash'),
  del      = require('del')
;

let reload = () => serve.reload();
let stream = () => serve.stream();
let root = 'frontend';

// helper method for resolving paths
let resolveToApp = (glob, name) => {
  glob = glob || '';
  return path.join(root, name, glob); // app/{glob}
};

let resolveToComponents = (glob, name) => {
  glob = glob || '';
  return path.join(root, `${name}`, glob);
};

// map of all paths
let paths = {
  js: [
    resolveToComponents('**/*!(.spec.js).js', 'common'),
    resolveToComponents('**/*!(.spec.js).js', 'app')
  ], // exclude spec files
  less: [
    resolveToApp('**/*.less', 'common'),
    resolveToApp('**/*.less', 'app'),
    resolveToApp('**/*.less', 'styles')
  ], // stylesheets
  html: [
    resolveToApp('**/*.jade', 'common'),
    resolveToApp('**/*.jade', 'app'),
    path.join(root, 'index.html')
  ],
  entry: path.join(root, 'app/app.js'),
  output: 'web/frontend'
};

gulp.task('clear', (cb) => {

  return del([
    paths.output + '/assets/**'
  ]);
});

gulp.task('markups', ['webpack', 'styles', 'assets'], () => {

  return gulp.src([
      path.join(root, 'index.html')
    ])
    .pipe(injectScripts('app'))
    .pipe(injectScripts('init', true))
    .pipe(gulp.dest(paths.output))
  ;
});

gulp.task('build', (done) => {
  sync(['clear', 'markups'], done);
});

// use webpack.config.js to build modules
gulp.task('webpack', () => {

  return gulp.src([paths.entry])
    .pipe(named())
    .pipe(webpack(require('./webpack.config.js')))
    .pipe(gulp.dest(paths.output + '/assets'));
});

gulp.task('assets', () => {
  return gulp.src([
      resolveToComponents('**/*', 'images'),
      resolveToComponents('**/*', 'fonts')
    ],{ "base" : root })
    .pipe(gulp.dest(paths.output));
});

gulp.task('styles', () => {

  return gulp.src([
      resolveToComponents('app.less', 'styles'),
      resolveToComponents('admin.less', 'styles')
    ])
    .pipe(sourcemaps.init())
    .pipe(less())
    .pipe(plumber({ errorHandler: onError }))
    .on('error', onError)
    .pipe(postcss([
      autoprefixer({browsers: ['last 2 version', 'ie >= 10']})
      //assets({
      //  loadPaths: ['frontend/images']
      //})
    ]))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest(paths.output + '/assets'))
    .pipe(serve.stream())
  ;

});

gulp.task('serve', () => {

  serve({
    port: process.env.PORT || 3000,
    open: false,
    server: {
      baseDir: paths.output,
      directory: false,
      https: false,
      middleware: [
        httpProxy(['/api', '/app_dev.php/api', '/socket.io'], {
          target: 'http://rbkgtest.vagrant'
        }),
        rewriteMiddleware([
          { pattern: /^.*\/[^\.]+$/, target: '/index.html' }
        ])
      ]
    }
  });
});

gulp.task('watch', () => {
  let allPaths = [].concat([paths.js], paths.html);
  gulp.watch(allPaths, ['webpack', reload]);
  gulp.watch(paths.less, ['styles']);
});

gulp.task('default', (done) => {
  sync('clear', 'build', 'serve', 'watch', done);
});

function rewriteMiddleware(rewrites) {

  return function (req, res, next) {
    rewrites.some((rule) => {
      let result;
      if (result = rule.pattern.test(req.url)) {
        req.url = req.url.replace(rule.pattern, rule.target);
      }

      return result;
    });

    next();
  }
}

function injectScripts(name, inline) {
  inline = inline === true;

  let scripts;
  if ('init' === name) {
    scripts = ['init'];
  } else {
    scripts = ['vendor.*.bundle', `${name}.*.bundle`];
  }

  return inject(
    gulp.src(scripts.map((name) => `${paths.output}/assets/${name}.js`), {read: inline}), {
      name,
      transform: inline ? inlineContents : null,
      ignorePath: paths.output,
      addRootSlash: false
    }
  );
}

function inlineContents(filePath, file) {

  // return file contents as string
  return ['<script>', file.contents.toString('utf8'), '</script>'].join('\n');
}

var onError = function(err) {
  console.error('handled error', err);
  this.emit('end');
};
