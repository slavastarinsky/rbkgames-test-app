const webpack = require('webpack');
const argv = require('yargs').argv;

var plugins = [
    new webpack.optimize.CommonsChunkPlugin({
        name: "vendor", minChunks: Infinity
    }),
    new webpack.optimize.CommonsChunkPlugin("init.js")
];

if (argv.release) {
    plugins.push(
        new webpack.optimize.UglifyJsPlugin({
            mangle: {
                except: ['$super', '$', 'exports', 'require', 'moment', 'angular']
            }
        })
    );
}

module.exports = {
  devtool: 'sourcemap',

  entry: {
    app: 'app/app.js',
    vendor: ['angular', "angular-ui-router", 'angular-ui-bootstrap', 'moment', 'lodash']
  },

  resolve: {
    root: __dirname + '/frontend',
    alias: {
      'app-config': `./../config/${argv.release ? 'release' : 'local' }.js`,
      'common': `${__dirname}/frontend/common`,
      'app': `${__dirname}/frontend/app`
    }
  },

  externals: {
    angular: 'angular'
  },

  output: {
    filename: `[name].${argv.release ? '[hash]' : 'hash'}.bundle.js`
  },
  module: {
    loaders: [
       { test: /\.js$/, exclude: [/app\/lib/, /node_modules/], loader: 'ng-annotate!babel' },
       { test: /\.jade$/, loader: "jade" },
       { test: /\.html$/, loader: 'raw' },
       { test: /\.less$/, loader: "style!css!less" },
       { test: /\.css$/, loader: 'style!css' }
    ]
  },

  plugins: plugins
};
