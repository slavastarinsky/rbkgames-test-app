RBKGames Test App
===================================

## Local deployment and provisioning

Run `vagrant up --provision` for the local deployment.

## Load rates via CLI

For the first use load rates for any date via CLI command `rbkg:exchange:parse <date>`.

## Exchange via CLI

You can check exchange functionality via CLI command `rbkg:exchange:parse <date> <currency_from> <currency_to> <amount>`

## Compiling frontend

Compile frontend inside vm via command `gulp build`